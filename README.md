Projekt układanki z lat dziecinnych to graficzna aplikacja wykorzystującą bibliotekę wxWidgets do realizacji swojej części graficznej.
Program odtwarza układankę, składającą się z z n * n pól w kwadracie o boku n. Celem jest uszeregowanie pól w kolejności rosnącej.

Program składa się z następujących części graficznych:
-Menu startowego, zawierającego przyciski Nowa gra, Wyniki oraz Wyjdź
-Okna dialogowego opcji generacji układanki, posiadającego pola do określenia rozmiaru oraz ruchów które mają pomieszać planszę, oraz przycisku Start
-Okna dialogowego wyników, wyświetlającego datę oraz ilość ruchów poprzednich zwycięskich podejść
-Okna gry, wyświetlającego kafelki układanki, oraz pasek statusu informujący ile ruchów zostało wykonanych, obsługujące klawisze strzałek do poruszania kafelkami
-Okna dialogowego wieńczącego zwycięskie podejście, wyświetlające ilość ruchów w których ukonczyliśmy grę, oraz przycisk kończący działanie programu.

Szczegółowo:
Menu startowe - jest to obiekt klasy **fStartWindow**
Nowa gra - kliknięcie przycisku, wywołuje metodę **OnStart()**, która tworzy menu dialogowe **dGenerateDialog**
Wyniki - kliknięcie, wywołuje metodę **OnScores()**, która wczytuje tekst z pliku płaskiego _scores_ a następnie tworzy okno dialogowe **dScoresDialog** i prezkazuje wczytany tekst w parametrze
Wyjdź - kliknięcie wywołuje metodę **Close()**, która wchodzi w skład _wxWidgets_ i obsługuje niszczenie okien graficznych

Okno dialogowe generacji - obiekt klasy **dGenerateDialog**
Start - kliknięcie przycisku wywołuje metodę **OnStart()** walidującą wpisany rozmiar układanki i w razie potrzeby ustawia na domyślny rozmiar  n = 4, tworzy obiek klasy **fGameWindow** przekazując mu rozmiar planszy i podane ruchy, a na koniec niszczy siebie oraz menu startowe.
W konstruktorze klasy **fGameWindow** tworzony jest również obiekt klasy **PuzzleBoard**, ktoremu przkazane zostają rozmiar i ruchy, obiekt klasy **HumanController**, któremu przekazane są wskaźniki na **PuzzleBoard** i **fGameWindow**.
Następnie tworzony jest pasek statusu, przyciski symbolizujące kafelki układanki i ostatecznie jest to dodawane do okna gry.

W oknie gry, przechwytywane są eventy wciśnięcia klawiszy i w ich obsłudze w metodzie **OnKeyDown()**, dla klawiszy strzałek wywoływana jest odpowiednia metoda obieku klasy **HumanController** **movePuzzle()** z argumentem 0-3 wskazującym kierunek ruchu.

W tej metodzie, najpierw wywoływana jest metoda klasy bazowej **Controller** **movePiece()** która odpowiednio działa na modelu, czyli klasie **PuzzleBoard** i zmienia stan układanki.
Następnie dalej w metodzie **movePuzzle**, uaktualniany jest graficzny stan na kafelkach poprzed metodę **fGameWindow** **UpdatePuzzlesLabels()**. Dalej uaktualniany jest licznik ruchów **updateCounter()** oraz sprawdzane kryterium wygranej **CheckWin()**.
Jeżeli to kryterium jest spełnione wywoływana jest metoda **ShowWonDialog()**

Okno dialogowe wygranej, tworzone jest w metodzie **ShowWonDialog()**. W nim zapisywana jest aktualna data i czas, ilość ruchów, a następnie te informacje są zapisywane do pliku płaskiego scores po kliknięciu w przycisk Koniec, który powiązany jest z metodą **OnWin()** i który na koniec niszczy siebie i okno gry.