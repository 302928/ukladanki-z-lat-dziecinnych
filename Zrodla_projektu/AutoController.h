#ifndef AUTOCONTROLLER_H
#define AUTOCONTROLLER_H
#include "Controller.h"
#include "wx/wx.h"

class AutoController : public Controller{
    public:
    AutoController(PuzzleBoard *puzzleBoard, fGameWindow *gameWindow);
    virtual void movePuzzle(int dir);
};
#endif