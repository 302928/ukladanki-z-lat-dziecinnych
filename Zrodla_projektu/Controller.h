#ifndef CONTROLLER_H
#define CONTROLLER_H
class PuzzleBoard;
class fGameWindow;

class Controller {
    public:
    Controller(PuzzleBoard *puzzleBoard, fGameWindow *gameWindow);
    void movePiece(char cMove);
    void movePiece(int iMove);
    void CheckWin();
    bool HasWon();
    virtual void movePuzzle(int dir) = 0;

    protected:
    PuzzleBoard *pBoard;
    fGameWindow *gWindow;
    bool won;
};
#endif