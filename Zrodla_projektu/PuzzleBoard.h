#ifndef PUZZLEBOARD_H
#define PUZZLEBOARD_H

#include <string>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h> 
#include "Tile.h"

class PuzzleBoard{
    public:
    PuzzleBoard(int size, std::string moves);
    int getSize();
    std::string GetPieceLabelAt(int x, int y);
    std::string GetSolvedPieceLabelAt(int x, int y);
    void SetPieceLabelAt(int x, int y, std::string label);
    int GetXCoord();
    int GetYCoord();
    void SetXCoord(int x);
    void SetYCoord(int y);

    private:
    int boardSize;
    int xCoord;
    int yCoord;

    std::string shuffleMoves;
    Tile **solvedState;
    Tile **scrambledState;

    void GenerateShuffle();
    void ShuffleBoard();
    void movePiecesToShuffle(char cMove);

};
#endif