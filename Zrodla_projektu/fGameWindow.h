#ifndef FGAMEWINDOW_H
#define FGAMEWINDOW_H
#include <wx/wx.h>
#include "PuzzleBoard.h"
#include "Controller.h"
#include <string>


class fGameWindow : public wxFrame
{
  public:
    fGameWindow(const wxString& title, int boardSize, std::string shuffleMoves);
    void updateCounter();
    void UpdatePuzzlesLabels(int x, int y, std::string label);
    void ShowWonDialog();
    int getMoveCount();
    void Quit();

protected:
    void OnKeyDown(wxKeyEvent& event);
    void OnAuto(wxCommandEvent & event);

private:

    int moveCounter;
    wxStatusBar *m_stsbar;
    PuzzleBoard *puzzleBoard;
    Controller *hControl;
    Controller *aControl;
    wxButton **bPuzzleButtons;

    wxString str;
    wxBoxSizer *vbox;
    wxGridSizer *grid;
};
#endif