#include <wx/wx.h>
#include "fGameWindow.h"
class dWonDialog : public wxDialog
{
public:
  dWonDialog(const wxString& title, fGameWindow* fromWho);
protected:
    void OnWin(wxCommandEvent & event);
private:
    wxPanel* panel;
 
    fGameWindow* originatorFrame;
    wxDateTime now;
};
