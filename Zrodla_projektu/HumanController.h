#ifndef HUMANCONTROLLER_H
#define HUMANCONTROLLER_H
#include "Controller.h"
#include "wx/wx.h"

class HumanController : public Controller{
    public:
    HumanController(PuzzleBoard *puzzleBoard, fGameWindow *gameWindow);
    virtual void movePuzzle(int dir);
};
#endif