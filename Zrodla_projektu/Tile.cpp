#include "Tile.h"

Tile::Tile(std::string value)
{
    tileValue = value;
}

std::string Tile::getValue()
{
    return tileValue;
}

void Tile::setValue(std::string val)
{
    tileValue = val;
}