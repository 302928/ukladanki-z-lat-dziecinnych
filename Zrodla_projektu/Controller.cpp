#include "Controller.h"

#include "PuzzleBoard.h"
#include "fGameWindow.h"

Controller::Controller(PuzzleBoard *puzzleBoard, fGameWindow *gameWindow){
    pBoard = puzzleBoard;
    gWindow = gameWindow;

    this->won = false;
}

void Controller::movePiece(char cMove){
    std::string tmpLabel = "";
    int x = pBoard->GetXCoord();
    int y = pBoard->GetYCoord();
    int size = pBoard->getSize();

    if(cMove=='L' or cMove=='l'){
        if(x-1>=0){
            tmpLabel = pBoard->GetPieceLabelAt(x-1, y);
            pBoard->SetPieceLabelAt(x-1, y, pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetXCoord(x-1);
        }
    }
    if(cMove=='R' or cMove=='r'){
        if(x+1<=size-1){
            tmpLabel = pBoard->GetPieceLabelAt(x+1, y);
            pBoard->SetPieceLabelAt(x+1, y, pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetXCoord(x+1);
        }
    }
    if(cMove=='U' or cMove=='u'){
        if(y-1>=0){
            tmpLabel = pBoard->GetPieceLabelAt(x, (y-1));
            pBoard->SetPieceLabelAt(x, (y-1), pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetYCoord(y-1);
        }
    }
    if(cMove=='D' or cMove=='d'){
        if(y+1<=size-1){
            tmpLabel = pBoard->GetPieceLabelAt(x, (y+1));
            pBoard->SetPieceLabelAt(x, (y+1), pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetYCoord(y+1);
        }
    }
}

void Controller::movePiece(int iMove){
    std::string tmpLabel = "";
    int x = pBoard->GetXCoord();
    int y = pBoard->GetYCoord();
    int size = pBoard->getSize();

    if(iMove==0){
        if(x-1>=0){
            tmpLabel = pBoard->GetPieceLabelAt(x-1, y);
            pBoard->SetPieceLabelAt(x-1, y, pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetXCoord(x-1);
        }
    }
    if(iMove==2){
        if(x+1<=size-1){
            tmpLabel = pBoard->GetPieceLabelAt(x+1, y);
            pBoard->SetPieceLabelAt(x+1, y, pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetXCoord(x+1);
        }
    }
    if(iMove==1){
        if(y-1>=0){
            tmpLabel = pBoard->GetPieceLabelAt(x, (y-1));
            pBoard->SetPieceLabelAt(x, (y-1), pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetYCoord(y-1);
        }
    }
    if(iMove==3){
        if(y+1<=size-1){
            tmpLabel = pBoard->GetPieceLabelAt(x, (y+1));
            pBoard->SetPieceLabelAt(x, (y+1), pBoard->GetPieceLabelAt(x, y));
            pBoard->SetPieceLabelAt(x, y, tmpLabel);
            pBoard->SetYCoord(y+1);
        }
    }
}

void Controller::CheckWin()
{
    int size = pBoard->getSize();
    won = true;
    for (int y=0; y<size; y++){
        for (int x = 0; x<size; x++){
            if(pBoard->GetSolvedPieceLabelAt(x, y) != pBoard->GetPieceLabelAt(x, y)){
                won = false;
            } 
        }
    }
}

bool Controller::HasWon(){
    return won;
}