#include "main.h"
#include "fStartWindow.h"

IMPLEMENT_APP(ProjectApp)

bool ProjectApp::OnInit()
{
    fStartWindow *startWindow = new fStartWindow(wxT("Gra układanka"));
    startWindow->Show(true);
    return true;
}
