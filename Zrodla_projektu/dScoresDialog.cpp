#include "dScoresDialog.h"

dScoresDialog::dScoresDialog(const wxString & title, wxString scoresText)
       : wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(300, 230))
{

    wxPanel *panel = new wxPanel(this, -1);

    wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

    wxStaticText *scores = new wxStaticText(panel, -1, scoresText);
    
    vbox->Add(panel, 1);

    SetSizer(vbox);

    Centre();
    ShowModal();

    Destroy(); 
}