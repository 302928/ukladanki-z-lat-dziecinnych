#include "dGenerateDialog.h"
#include "fGameWindow.h"

dGenerateDialog::dGenerateDialog(const wxString & title, wxFrame* fromWho)
       : wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(250, 230))
{
    originatorFrame = fromWho;

    panel = new wxPanel(this, -1);
    sizeLabel = new wxStaticText(panel, -1, wxT("Podaj rozmiar planszy"), wxPoint(40, 30));
    boardSize = new wxTextCtrl(panel, -1, wxT(""), wxPoint(95, 45));
    movesLabel = new wxStaticText(panel, -1, wxT("Podaj ruchy (LUPD) "), wxPoint(40, 90));
    preMoves = new wxTextCtrl(panel, -1, wxT(""), wxPoint(95, 105));
    wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

    wxButton *okButton = new wxButton(this, 10100, wxT("Start"), wxDefaultPosition, wxSize(70, 30));

    hbox->Add(okButton, 1);
    vbox->Add(panel, 1);
    vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

    SetSizer(vbox);

    Centre();
    Connect(10100, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(dGenerateDialog::OnStart));
    ShowModal(); 
}
void dGenerateDialog::OnStart(wxCommandEvent & event)
{
    int size;
    std::string moves;
    try
    {
        size = std::stoi(boardSize->GetValue().ToStdString());
    }
    catch(const std::exception& e)
    {
        size = 4;
    }
    moves = preMoves->GetValue().ToStdString();
    
    fGameWindow *gameWindow = new fGameWindow(wxT("Ułóż puzzle"),size,moves);
    gameWindow->Show(true);
    originatorFrame->Close(true);
    Destroy();
    event.Skip();
}