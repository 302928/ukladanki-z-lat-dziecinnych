#include "PuzzleBoard.h"

PuzzleBoard::PuzzleBoard(int boardSize, std::string moves){

    this->boardSize = boardSize;
    this->solvedState = new Tile*[boardSize * boardSize];
    this->scrambledState = new Tile*[boardSize * boardSize];
    this->shuffleMoves = moves;
    for (int yCoord=0; yCoord<boardSize; yCoord++){
        for (int xCoord = 0; xCoord<boardSize; xCoord++){
            solvedState[yCoord*boardSize + xCoord] = new Tile(std::to_string(yCoord*boardSize + xCoord+1));
            scrambledState[yCoord*boardSize + xCoord] = new Tile(std::to_string(yCoord*boardSize + xCoord+1));
        }
    }
    xCoord = boardSize-1;
    yCoord = boardSize-1;
    solvedState[boardSize*boardSize -1]->setValue(" ") ;
    scrambledState[boardSize*boardSize -1]->setValue(" ") ;   

    GenerateShuffle();

    ShuffleBoard();
    
}

int PuzzleBoard::getSize(){
    return boardSize;
}

void PuzzleBoard::GenerateShuffle(){
    if(shuffleMoves.empty()){
        srand (time(NULL));
        int randNumber;
        for (int i = 0; i<20; i++){
            randNumber = rand() % 8;
            switch (randNumber){
            case 0:

            case 4:
            shuffleMoves.append("L");
                break;
            case 1:

            case 5:
            shuffleMoves.append("U");
                break;
            case 2:

            case 6:
            shuffleMoves.append("R");
                break;
            case 3:

            case 7:
            shuffleMoves.append("D");
                break;
            default:
                break;
            }
        }
    }
    else{
        std::string tmpMoves = "";
        for (int i =0; i<shuffleMoves.length(); i++){
            if(shuffleMoves[i]=='L' or shuffleMoves[i]=='l' or shuffleMoves[i]=='U' or shuffleMoves[i]=='u' or 
            shuffleMoves[i]=='R' or shuffleMoves[i]=='r' or shuffleMoves[i]=='D' or shuffleMoves[i]=='d'){
                tmpMoves += shuffleMoves.at(i);
            }
        }
        shuffleMoves = tmpMoves;
    }
}

void PuzzleBoard::ShuffleBoard(){
        for (int i =0; i<shuffleMoves.length(); i++){
           movePiecesToShuffle(shuffleMoves[i]);
        }
}
std::string PuzzleBoard::GetPieceLabelAt(int xCoord, int yCoord){
    return scrambledState[yCoord*boardSize + xCoord]->getValue();
}
void PuzzleBoard::SetPieceLabelAt(int xCoord, int yCoord, std::string label){
    scrambledState[yCoord*boardSize + xCoord]->setValue(label);
}
std::string PuzzleBoard::GetSolvedPieceLabelAt(int xCoord, int yCoord){
    return solvedState[yCoord*boardSize + xCoord]->getValue();
}
int PuzzleBoard::GetXCoord(){
    return xCoord;
}
int PuzzleBoard::GetYCoord(){
    return yCoord;
}
void PuzzleBoard::SetXCoord(int x){
    xCoord = x;
}
void PuzzleBoard::SetYCoord(int y){
    yCoord = y;
}
void PuzzleBoard::movePiecesToShuffle(char cMove){
    std::string tmpLabel = "";
    std::string tmp2Label = "";
    tmp2Label = scrambledState[yCoord*boardSize + xCoord]->getValue();
    if(cMove=='L' or cMove=='l'){
        if(xCoord-1>=0){
            tmpLabel = scrambledState[yCoord*boardSize + xCoord-1]->getValue();
            scrambledState[yCoord*boardSize + xCoord-1]->setValue(tmp2Label);
            scrambledState[yCoord*boardSize + xCoord]->setValue(tmpLabel);
            SetXCoord(xCoord-1);
        }
    }
    if(cMove=='R' or cMove=='r'){
        if(xCoord+1<=boardSize-1){
            tmpLabel = scrambledState[yCoord*boardSize + xCoord+1]->getValue();
            scrambledState[yCoord*boardSize + xCoord+1]->setValue(tmp2Label);
            scrambledState[yCoord*boardSize + xCoord]->setValue(tmpLabel);
            SetXCoord(xCoord+1);
        }
    }
    if(cMove=='U' or cMove=='u'){
        if(yCoord-1>=0){
            tmpLabel = scrambledState[(yCoord-1)*boardSize + xCoord]->getValue();
            scrambledState[(yCoord-1)*boardSize + xCoord]->setValue(tmp2Label);
            scrambledState[yCoord*boardSize + xCoord]->setValue(tmpLabel);
            SetYCoord(yCoord-1);
        }
    }
    if(cMove=='D' or cMove=='d'){
        if(yCoord+1<=boardSize-1){
            tmpLabel = scrambledState[(yCoord+1)*boardSize + xCoord]->getValue();
            scrambledState[(yCoord+1)*boardSize + xCoord]->setValue(tmp2Label);
            scrambledState[yCoord*boardSize + xCoord]->setValue(tmpLabel);
            SetYCoord(yCoord+1);
        }
    }
}