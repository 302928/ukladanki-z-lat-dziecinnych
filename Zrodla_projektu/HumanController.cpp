#include "HumanController.h"
#include "fGameWindow.h"
#include "PuzzleBoard.h"

HumanController::HumanController(PuzzleBoard *puzzleBoard, fGameWindow *gameWindow) : Controller(puzzleBoard, gameWindow){
    this->pBoard = puzzleBoard;
    this->gWindow = gameWindow;
}

void HumanController::movePuzzle(int dir)
{

    this->movePiece(dir);
    for (int y=0; y<pBoard->getSize(); y++)
    {
        for (int x = 0; x<pBoard->getSize(); x++)
        {
            gWindow->UpdatePuzzlesLabels(x, y*pBoard->getSize(), pBoard->GetPieceLabelAt(x,y));
        }
    }
        gWindow->updateCounter();
        this->CheckWin();
    if(this->HasWon()){
        gWindow->ShowWonDialog();
    }
}