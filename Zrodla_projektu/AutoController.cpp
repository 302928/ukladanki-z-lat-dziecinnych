#include "AutoController.h"
#include "fGameWindow.h"
#include "PuzzleBoard.h"

AutoController::AutoController(PuzzleBoard *puzzleBoard, fGameWindow *gameWindow) : Controller(puzzleBoard, gameWindow){
    this->pBoard = puzzleBoard;
    this->gWindow = gameWindow;
}

void AutoController::movePuzzle(int dir)
{
    int randNumber;
    for(int i =0; i < 16; i++)
    {
        randNumber = rand() % 4;
        this->movePiece(randNumber);
        for (int y=0; y<pBoard->getSize(); y++)
        {
            for (int x = 0; x<pBoard->getSize(); x++)
            {
                gWindow->UpdatePuzzlesLabels(x, y*pBoard->getSize(), pBoard->GetPieceLabelAt(x,y));
            }
        }
        gWindow->updateCounter();
        this->CheckWin();
        if(this->HasWon()){
            break;
        }
    }
    if(this->HasWon()){
        gWindow->ShowWonDialog();
    }
}