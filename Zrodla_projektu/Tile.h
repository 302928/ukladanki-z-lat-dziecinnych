#ifndef TILE_H
#define TILE_H

#include <string>

class Tile {
    public:
    Tile(std::string value);
    void setValue(std::string val);
    std::string getValue();
    private:
    std::string tileValue;
};
#endif