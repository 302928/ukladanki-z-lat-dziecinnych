#include "fGameWindow.h"
#include "dWonDialog.h"
#include "AutoController.h"
#include "HumanController.h"
#include <string>

fGameWindow::fGameWindow(const wxString& title, int boardSize, std::string shuffleMoves)
       : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(290, 220))
{
    str = "";

    puzzleBoard = new PuzzleBoard(boardSize, shuffleMoves);

    moveCounter = 0;
;
    m_stsbar = CreateStatusBar(2);

    hControl = new HumanController(puzzleBoard, this);
    aControl = new AutoController(puzzleBoard, this);
    str = std::to_string(moveCounter);
    SetStatusText(wxT("Ilość ruchów"), 0);
    SetStatusText(str, 1);
    
    bPuzzleButtons = new wxButton*[boardSize * boardSize];
    
    vbox = new wxBoxSizer(wxVERTICAL);
    grid = new wxGridSizer(boardSize, boardSize, 5, 5);
    for (int y=0; y<boardSize; y++)
    {
        for (int x = 0; x<boardSize; x++)
        {
            bPuzzleButtons[y*boardSize + x] = new wxButton(this, 11000 + (y*boardSize +x), puzzleBoard->GetPieceLabelAt(x,y));
            grid->Add(bPuzzleButtons[y*boardSize + x], 1, wxEXPAND | wxALL);
        }
    }
    wxButton* bAutoButton = new wxButton(this, 12345, wxT("Auto"), wxDefaultPosition);
    Connect(12345, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(fGameWindow::OnAuto));
    vbox->Add(grid, 1, wxEXPAND | wxALL);
    vbox->Add(bAutoButton,1,wxEXPAND | wxALL);
    this->SetSizer(vbox);
    vbox->Layout();
    this->Bind(wxEVT_CHAR_HOOK, &fGameWindow::OnKeyDown, this);
}

void fGameWindow::OnKeyDown(wxKeyEvent& event)
{
    int keycode = event.GetKeyCode();

    switch (keycode) {
    case WXK_LEFT:
        hControl->movePuzzle(0);
        break;
    case WXK_RIGHT:
        hControl->movePuzzle(2);
        break;
    case WXK_DOWN:
        hControl->movePuzzle(3);
        break;
    case WXK_UP:
        hControl->movePuzzle(1);
        break;
    }
}

void fGameWindow::OnAuto(wxCommandEvent & event)
{
    aControl->movePuzzle(-1);
}

void fGameWindow::updateCounter()
{
    moveCounter+=1;
    str = std::to_string(moveCounter);
    SetStatusText(str, 1);
}

void fGameWindow::UpdatePuzzlesLabels(int x, int y, std::string label){
    bPuzzleButtons[y + x]->SetLabel(label);
}

int fGameWindow::getMoveCount(){
    return moveCounter;
}

void fGameWindow::ShowWonDialog(){
    dWonDialog *dial = new dWonDialog("Układanka ułożona",this);
}
void fGameWindow::Quit(){
    wxFrame::Close();
}