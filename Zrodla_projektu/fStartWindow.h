#include <wx/wx.h>

class fStartWindow : public wxFrame
{
    private:
    wxButton *bStartButton;
    wxButton *bScoreButton;
    wxButton *bExitButton;

    wxBoxSizer *boxSizer;
    wxGridSizer *gridSizer;
    protected:
    void OnStart(wxCommandEvent & event);
    void OnScores(wxCommandEvent & event);
    void OnExit(wxCommandEvent & event);
    public:
    fStartWindow(const wxString& title);
};