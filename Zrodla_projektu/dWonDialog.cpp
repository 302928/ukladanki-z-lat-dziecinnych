#include <wx/datetime.h>
#include <wx/textfile.h>
#include "dWonDialog.h"


dWonDialog::dWonDialog(const wxString & title, fGameWindow* fromWho)
       : wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(130, 240))
{
    originatorFrame = fromWho;
    now = wxDateTime::Now();
    panel = new wxPanel(this, -1);
    wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);
    wxString tmp = "";
    tmp.append(wxT("Ilość ruchów: "));
    tmp.append(std::to_string(originatorFrame->getMoveCount()));
    wxStaticText *sText = new wxStaticText(this, -1, tmp, wxPoint(5, 60));
    wxButton *closeButton = new wxButton(this, 10100, wxT("Koniec"), wxDefaultPosition, wxSize(70, 30));

    hbox->Add(closeButton, 1);
    vbox->Add(panel, 1);
    vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

    SetSizer(vbox);

    Centre();
    Connect(10100, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(dWonDialog::OnWin));
    ShowModal(); 
}
void dWonDialog::OnWin(wxCommandEvent & event)
{
    wxDateTime now = wxDateTime::Now();
    wxString finDate = now.Format();
    finDate.append(wxT(" - Ilość kroków: "));
    finDate.append(std::to_string(originatorFrame->getMoveCount()));
    wxTextFile file(wxT("scores"));
    file.Open();
    file.AddLine(finDate);
    file.Write();
    file.Close();
    Destroy();
    originatorFrame->Quit();
    
    event.Skip();
}