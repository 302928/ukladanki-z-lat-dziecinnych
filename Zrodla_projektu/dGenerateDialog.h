#include <wx/wx.h>

class dGenerateDialog : public wxDialog
{
public:
  dGenerateDialog(const wxString& title, wxFrame* fromWho);
protected:
    void OnStart(wxCommandEvent & event);
private:
    wxPanel* panel;
    wxTextCtrl* boardSize;
    wxTextCtrl* preMoves;
    wxStaticText *sizeLabel;
    wxStaticText *movesLabel;
    wxFrame* originatorFrame;
};
