#include "fStartWindow.h"
#include "fGameWindow.h"
#include "dScoresDialog.h"
#include "dGenerateDialog.h"
#include <wx/textfile.h>

fStartWindow::fStartWindow(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(290, 220))
       {
            bStartButton = new wxButton(this, 10001, wxT("Nowa gra"), wxDefaultPosition);
            bScoreButton = new wxButton(this, 10002, wxT("Wyniki"), wxDefaultPosition);
            bExitButton = new wxButton(this, 10003, wxT("Wyjdź"), wxDefaultPosition);

            Connect(10001, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(fStartWindow::OnStart));
            Connect(10002, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(fStartWindow::OnScores));
            Connect(10003, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(fStartWindow::OnExit));

            boxSizer = new wxBoxSizer(wxVERTICAL);
            boxSizer->Add(bStartButton,1, wxEXPAND);
            boxSizer->Add(bScoreButton,1, wxEXPAND);
            boxSizer->Add(bExitButton,1, wxEXPAND);
            SetSizer(boxSizer);
            Centre();
       }

void fStartWindow::OnStart(wxCommandEvent & event)
{
    dGenerateDialog *dial = new dGenerateDialog("Ustaw opcje",this);
}
    
void fStartWindow::OnScores(wxCommandEvent & event)
{
    wxTextFile file(wxT("scores"));

    file.Open();
    wxString s;
    wxString scoresText;

    for ( s = file.GetFirstLine(); !file.Eof(); 
        s = file.GetNextLine() )
    {
        scoresText.Append(s);
        scoresText.Append("\n");
    }

    file.Close();
        dScoresDialog *dial = new dScoresDialog("Wyniki", scoresText);
}

void fStartWindow::OnExit(wxCommandEvent & event)
{
        this->Close();
}